<?php

class controller
{
	var $css;
	var $js;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();

		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
				
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
	    (include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
            
    
	protected function redirect($page)
	{
		header( 'Location: '.BASE_URL.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{
		$css[] = "/public/css/zoom_box/zoom_box.css";
		$css[] = "/public/css/style.css";
		$css[] = "/public/css/respond.css";
		$css[] = "/public/font-awesome-4.3.0/css/font-awesome.min.css";
		$css[] = "/punlic/css/background.css";
		
		//$font[] = "http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css";
		$font[] = "http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600";
		$font[] = "http://fonts.googleapis.com/css?family=Six+Caps";
		
		$js[] = "/public/scripts/jquery.js";
		$js[] = "/public/scripts/jquery-migrate.min.js";
		$js[] = "/public/css/zoom_box/zoom_box.js";
		$js[] = "/public/scripts/core.js";
		$js[] = "/public/scripts/transition.js";
		$js[] = "/public/scripts/background.js";
		$js[] = "/public/scripts/spin.js";
		$js[] = "/public/scripts/custom.js";
		$js[] = "/public/scripts/retina.js";
		
		
		$cekip = $_SERVER['REMOTE_ADDR'];	
		helper::getVisitor($cekip);
		
		
		$menuatas = $this->getView(DOCVIEW.'template/header_menu.php', $var);
		$var['menuatas'] = $menuatas;	
		
		$controller = $group;
		
		$content = $this->getView(DOCVIEW.$view, $var);
		
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);
		//if(strpos($_SERVER['HTTP_HOST'], 'dev') !== false)
			include(DOCVIEW."template/template.php");
		//elseif(($_SERVER['HTTP_HOST'] == 'bidahcinta.com') || ($_SERVER['HTTP_HOST'] == 'www.bidahcinta.com'));
		/*
		else
					include(DOCVIEW."template/maintenance/comingsoon.php");*/
		
	}
	
	protected function underrender($group,$view,$var)
	{
		include(DOCVIEW."under/index.php");
	}

}
?>