<?php

class gallery_admin extends admin {
	
	public function index() {
		$page = $this->getPage();
		$sort = $this->getSort();
		$tipesort = $this->getSortType();
		
		$limit = 10;
		$skip = (int)($limit * ($page-1));
		
		$search = isset($_SESSION['search_admingallery']) ? $_SESSION['search_admingallery'] : '';
		
		$arr = array();
		if(!empty($_POST)) {
			$search = trim($_POST['search']);			
			
			$_SESSION['search_admingallery'] = $search;
		}
		
		$query = array();
		if(strlen(trim($search)) > 0) {
			$regex = new MongoRegex('/'.$search.'/i');
			$query = array(
				'$or' => array(
				array('title' => $regex)
			));			
		}
		
		$db = Db::init();
		$col = $db->gallery;
		
		$data = $col->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
		$count = $col->count($query);
		
		$pg = new Pagination();
		$pg->pag_url = '/admin/gallery/index?page=';
		$pg->calculate_pages($count, $limit, $page);	
		
		$var = array(
			'sort' => $sort,
			'tipesort' => $tipesort,
			'data' => $data,
			'page' => $page,
			'judul' => ' Gallery List',
			'search' => $search,
			'link' => '/admin/gallery/index',
			'pagination' => $pg->Show()
		);

		$this->js[] = '/public/backend/controller/delete.js';

		$this->render("gallery", "admin/gallery/index.php", $var);
	}

	public function add() {
		$error = array();
		$title = '';
		$description = '';
		$namafile = '';
		$slider = '';
		
		if(!empty($_POST)) {
			$title = isset($_POST['title']) ? trim($_POST['title']) : '';
			$description = isset($_POST['description']) ? trim($_POST['description']) : '';
			$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
			$slider = isset($_POST['slider']) ? trim($_POST['slider']) : '';
			
			$validator = new Validator();
			$validator->addRule('title', array('require'));
			$validator->addRule('slider', array('require'));
			$validator->setData(array('title' => $title, 'slider' => $slider));
			
			if($validator->isValid()) {
				$foto = '';
	            $namafileshafoto = $namafile;
	            if(isset($_FILES['foto']['name']))
	            {
	                $foto = $_FILES['foto']['name'];
	                if(strlen(trim($foto)) > 0)
	                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
	            }
				
				$data = array(
					'title' => $title,
					'description' => $description,
					'created_by' => trim($_SESSION['userid']),
					'filename' => $namafileshafoto,
					'slider' => $slider,
					'time_created' => time()
				);
				
				$db = Db::init();
				$col = $db->gallery;
				$col->insert($data);
				
				if(strlen(trim($namafileshafoto)) > 0) {
					if(! is_dir(IMGPATH.'bidahcinta')) {
						if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
						    die('Failed to create folders...');
						}
					}
					move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
				}
				
				$this->redirect('/admin/gallery/index');
				exit;
			}
			else
				$error = $validator->getErrors();
		}

		$var = array(
			'error' => $error,
			'title' => $title,
			'description' => $description,
			'slider' => $slider,
			'namafile' => $namafile,
			'judul' => ' Add New Gallery',
			'link' => '/admin/gallery/add'
		);
		
		$this->render('gallery', 'admin/gallery/add.php', $var);
	}

	public function edit() {
		$page = $this->getPage();
		$id = $_GET['id'];
		$db = Db::init();
		$col = $db->gallery;
		$mcol = $col->findone(array('_id' => new MongoId($id)));
		
		$error = array();
		$title = isset($mcol['title']) ? trim($mcol['title']) : '';
		$description = isset($mcol['description']) ? trim($mcol['description']) : '';
		$namafile = isset($mcol['filename']) ? trim($mcol['filename']) : '';
		$slider = isset($mcol['slider']) ? trim($mcol['slider']) : '';
		
		if(!empty($_POST)) {
			$title = isset($_POST['title']) ? trim($_POST['title']) : '';
			$description = isset($_POST['description']) ? trim($_POST['description']) : '';
			$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
			$slider = isset($_POST['slider']) ? trim($_POST['slider']) : '';
			
			$validator = new Validator();
			$validator->addRule('title', array('require'));
			$validator->addRule('slider', array('require'));
			$validator->setData(array('title' => $title, 'slider' => $slider));
			
			if($validator->isValid()) {
				$foto = '';
	            $namafileshafoto = $namafile;
	            if(isset($_FILES['foto']['name']))
	            {
	                $foto = $_FILES['foto']['name'];
	                if(strlen(trim($foto)) > 0)
	                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
	            }
				
				$data = array(
					'title' => $title,
					'description' => $description,
					'filename' => $namafileshafoto,
					'slider' => $slider
				);
				
				$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
				
				if(isset($_FILES['foto']['name'])) {
					if($namafileshafoto !== $namafile) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						
						if(strlen(trim($namafile)) > 0) {
							if(file_exists(IMGPATH.'bidahcinta/'.$namafile)) {
								if(! unlink(IMGPATH.'bidahcinta/'.$namafile)){
									die('Failed to delete file...');
								}
							}
						}
						
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
					}
				}
				
				$this->redirect('/admin/gallery/index?page='.$page);
				exit;
			}
			else
				$error = $validator->getErrors();
		}

		$var = array(
			'error' => $error,
			'title' => $title,
			'description' => $description,
			'namafile' => $namafile,
			'judul' => ' Edit Gallery',
			'slider' => $slider,
			'link' => '/admin/gallery/edit?id='.$id.'&page='.$page
		);
		
		$this->render('gallery', 'admin/gallery/add.php', $var);
	}

	public function delete() {
		$id = $_GET['id'];
		$page = $this->getPage();
		
		$db = Db::init();
		$col = $db->gallery;
		
		$mcol = $col->findone(array('_id' => new MongoId($id)));
		if(isset($mcol['filename'])) {
			if(strlen(trim($mcol['filename'])) > 0) {
				if(file_exists(IMGPATH.'bidahcinta/'.$mcol['filename'])) {
					if(! unlink(IMGPATH.'bidahcinta/'.$mcol['filename'])){
						die('Failed to delete file...');
					}
				}
			}				
		}
		
		$col->remove(array('_id' => new MongoId($id)));
		
		$this->redirect('/admin/gallery/index?page='.$page);
	}
}
