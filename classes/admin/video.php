<?php
	class video_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_admingallery']) ? $_SESSION['search_admingallery'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_admingallery'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('title' => $regex)
				));			
			}
			
			$db = Db::init();
			$col = $db->videos;
			
			$data = $col->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $col->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/gallery/index?page=';
			$pg->calculate_pages($count, $limit, $page);	
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'page' => $page,
				'judul' => ' Video List',
				'search' => $search,
				'link' => '/admin/video/index',
				'pagination' => $pg->Show()
			);
	
			$this->js[] = '/public/backend/controller/delete.js';
	
			$this->render("video", "admin/video/index.php", $var);
		}
		
		public function add() {
			$error = array();
			$title = '';
			$description = '';
			$namafile = '';
			$linkvideo = '';
			
			if(!empty($_POST)) {
				$title = isset($_POST['title']) ? trim($_POST['title']) : '';
				$description = isset($_POST['description']) ? trim($_POST['description']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				$linkvideo = isset($_POST['linkvideo']) ? trim($_POST['linkvideo']) : '';
				
				$validator = new Validator();
				$validator->addRule('title', array('require'));
				$validator->addRule('linkvideo', array('require'));
				$validator->setData(array('title' => $title, 'linkvideo' => $linkvideo));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$data = array(
						'title' => $title,
						'description' => $description,
						'created_by' => trim($_SESSION['userid']),
						'filename' => $namafileshafoto,
						'linkvideo' => $linkvideo,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->videos;
					$col->insert($data);
					
					if(strlen(trim($namafileshafoto)) > 0) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
					}
					
					$this->redirect('/admin/video/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
	
			$var = array(
				'error' => $error,
				'title' => $title,
				'description' => $description,
				'linkvideo' => $linkvideo,
				'namafile' => $namafile,
				'judul' => ' Add New Video',
				'link' => '/admin/video/add'
			);
			
			$this->render('video', 'admin/video/add.php', $var);
		}

		public function edit() {
			$page = $this->getPage();
			$id = $_GET['id'];
			$db = Db::init();
			$col = $db->videos;
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			
			$error = array();
			$title = isset($mcol['title']) ? trim($mcol['title']) : '';
			$description = isset($mcol['description']) ? trim($mcol['description']) : '';
			$namafile = isset($mcol['filename']) ? trim($mcol['filename']) : '';
			$linkvideo = isset($mcol['linkvideo']) ? trim($mcol['linkvideo']) : '';
			
			if(!empty($_POST)) {
				$title = isset($_POST['title']) ? trim($_POST['title']) : '';
				$description = isset($_POST['description']) ? trim($_POST['description']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				$linkvideo = isset($_POST['linkvideo']) ? trim($_POST['linkvideo']) : '';
				
				$validator = new Validator();
				$validator->addRule('title', array('require'));
				$validator->addRule('linkvideo', array('require'));
				$validator->setData(array('title' => $title, 'linkvideo' => $linkvideo));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$data = array(
						'title' => $title,
						'description' => $description,
						'filename' => $namafileshafoto,
						'linkvideo' => $linkvideo
					);
					
					$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'bidahcinta')) {
								if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'bidahcinta/'.$namafile)) {
									if(! unlink(IMGPATH.'bidahcinta/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/video/index?page='.$page);
					exit;
				}
				else
					$error = $validator->getErrors();
			}
	
			$var = array(
				'error' => $error,
				'title' => $title,
				'description' => $description,
				'namafile' => $namafile,
				'judul' => ' Edit Gallery',
				'linkvideo' => $linkvideo,
				'link' => '/admin/video/edit?id='.$id.'&page='.$page
			);
			
			$this->render('video', 'admin/video/add.php', $var);
		}

		public function delete() {
			$id = $_GET['id'];
			$page = $this->getPage();
			
			$db = Db::init();
			$col = $db->videos;
			
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			if(isset($mcol['filename'])) {
				if(strlen(trim($mcol['filename'])) > 0) {
					if(file_exists(IMGPATH.'kaningapictures/'.$mcol['filename'])) {
						if(! unlink(IMGPATH.'kaningapictures/'.$mcol['filename'])){
							die('Failed to delete file...');
						}
					}
				}				
			}
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/videos/index?page='.$page);
		}
	}
?>