<?php
	class synopsis_admin extends admin{
		
		public function index(){
			$db = Db::init();
			$col = $db->synopsis;
			$mcol = $col->findone();
			
			$error = array();
			$narasi = isset($mcol['narasi']) ? trim($mcol['narasi']) : '';
			
			if(!empty($_POST)) {
				$narasi = isset($_POST['narasi']) ? trim($_POST['narasi']) : '';
				
				$validator = new Validator();
				$validator->addRule('narasi', array('require'));
				$validator->setData(array('narasi' => $narasi));
				
				if($validator->isValid()) {
					$data = array(
						'narasi' => $narasi
					);
					
					if(isset($mcol['_id']))
						$col->update(array('_id' => new MongoId($mcol['_id'])), array('$set' => $data));
					else {
						$data['time_created'] = time();
						$col->insert($data);
					}
					
					$this->redirect('/admin/synopsis/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'narasi' => $narasi,
				'judul' => ' Add / Edit Synopsis',
				'link' => '/admin/synopsis/index'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('synopsis', 'admin/synopsis/index.php', $var);
		}
	}
?>