<?php

class preference_admin extends admin {
	
	public function index() {
		$db = Db::init();
		$col = $db->preference;
		$mcol = $col->findone();
		
		$error = array();
		$name = isset($mcol['name']) ? trim($mcol['name']) : '';
		$address = isset($mcol['address']) ? trim($mcol['address']) : '';
		$email = isset($mcol['email']) ? trim($mcol['email']) : '';
		$phone = isset($mcol['phone']) ? trim($mcol['phone']) : '';
		$fax = isset($mcol['fax']) ? trim($mcol['fax']) : '';
		$narasi = isset($mcol['narasi']) ? trim($mcol['narasi']) : '';
		$namafile = isset($mcol['logo']) ? trim($mcol['logo']) : '';
		$namafile2 = isset($mcol['logo2']) ? trim($mcol['logo2']) : '';
		$namafile3 = isset($mcol['logo3']) ? trim($mcol['logo3']) : '';
		
		if(!empty($_POST)) {
			$name = isset($_POST['name']) ? trim($_POST['name']) : '';
			$address = isset($_POST['address']) ? trim($_POST['address']) : '';
			$email = isset($_POST['email']) ? trim($_POST['email']) : '';
			$phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
			$fax = isset($_POST['fax']) ? trim($_POST['fax']) : '';
			$narasi = isset($_POST['narasi']) ? trim($_POST['narasi']) : '';
			$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
			$namafile2 = isset($_POST['namafile2']) ? trim($_POST['namafile2']) : '';
			$namafile3 = isset($_POST['namafile3']) ? trim($_POST['namafile3']) : '';
			
			$validator = new Validator();
			$validator->addRule('name', array('require'));
			$validator->setData(array('name' => $name));
			
			if($validator->isValid()) {
				$foto = '';
	            $namafileshafoto = $namafile;
	            if(isset($_FILES['foto']['name']))
	            {
	                $foto = $_FILES['foto']['name'];
	                if(strlen(trim($foto)) > 0)
	                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
	            }
				
				$foto2 = '';
	            $namafileshafoto2 = $namafile2;
	            if(isset($_FILES['foto2']['name']))
	            {
	                $foto2 = $_FILES['foto2']['name'];
	                if(strlen(trim($foto2)) > 0)
	                    $namafileshafoto2 = sha1(date('Y-m-d H:i:s', time()).$foto2).'.'.helper::findexts($foto2);
	            }
				
				$foto3 = '';
	            $namafileshafoto3 = $namafile3;
	            if(isset($_FILES['foto3']['name']))
	            {
	                $foto3 = $_FILES['foto3']['name'];
	                if(strlen(trim($foto3)) > 0)
	                    $namafileshafoto3 = sha1(date('Y-m-d H:i:s', time()).$foto3).'.'.helper::findexts($foto3);
	            }
				
				$data = array(
					'name' => $name,
					'address' => $address,
					'email' => $email,
					'phone' => $phone,
					'fax' => $fax,
					'narasi' => $narasi,
					'logo' => $namafileshafoto,
					'logo2' => $namafileshafoto2,
					'logo3' => $namafileshafoto3
				);
				
				if(isset($mcol['_id']))
					$col->update(array('_id' => new MongoId($mcol['_id'])), array('$set' => $data));
				else {
					$data['time_created'] = time();
					$data['created_by'] = trim($_SESSION['userid']);
					$col->insert($data);
				}
				
				//---------------------------------------- logo 1 ----------------------------------------------
				if(isset($_FILES['foto']['name'])) {
					if($namafileshafoto !== $namafile) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						
						if(strlen(trim($namafile)) > 0) {
							if(file_exists(IMGPATH.'bidahcinta/'.$namafile)) {
								if(! unlink(IMGPATH.'bidahcinta/'.$namafile)){
									die('Failed to delete file...');
								}
							}
						}
						
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
					}
				}
				//---------------------------------------- end logo 1 -------------------------------------------
				
				//---------------------------------------- logo 2 ----------------------------------------------
				if(isset($_FILES['foto2']['name'])) {
					if($namafileshafoto2 !== $namafile2) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						
						if(strlen(trim($namafile2)) > 0) {
							if(file_exists(IMGPATH.'bidahcinta/'.$namafile2)) {
								if(! unlink(IMGPATH.'bidahcinta/'.$namafile2)){
									die('Failed to delete file...');
								}
							}
						}
						
						move_uploaded_file($_FILES['foto2']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto2);
					}
				}
				//---------------------------------------- end logo 2 -------------------------------------------
				
				//---------------------------------------- logo 3 ----------------------------------------------
				if(isset($_FILES['foto3']['name'])) {
					if($namafileshafoto3 !== $namafile3) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						
						if(strlen(trim($namafile3)) > 0) {
							if(file_exists(IMGPATH.'bidahcinta/'.$namafile3)) {
								if(! unlink(IMGPATH.'bidahcinta/'.$namafile3)){
									die('Failed to delete file...');
								}
							}
						}
						
						move_uploaded_file($_FILES['foto3']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto3);
					}
				}
				//---------------------------------------- end logo 3 -------------------------------------------
				
				$this->redirect('/admin/preference/index');
				exit;
			}
			else
				$error = $validator->getErrors();
		}
		
		$var = array(
			'error' => $error,
			'name' => $name,
			'address' => $address,
			'email' => $email,
			'phone' => $phone,
			'fax' => $fax,
			'narasi' => $narasi,
			'namafile' => $namafile,
			'namafile2' => $namafile2,
			'namafile3' => $namafile3,
			'judul' => ' Add / Edit Preference',
			'link' => '/admin/preference/index'
		);
		
		$this->render('preference', 'admin/preference/index.php', $var);
	}
}
