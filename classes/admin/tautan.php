<?php
	class tautan_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_admintautan']) ? $_SESSION['search_admintautan'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_admintautan'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('judul' => $regex)
				));			
			}
		
			$db = Db::init();
			
			$coltaut = $db->tautan;
			
			$data = $coltaut->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $coltaut->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/tautan/index?page=';
			$pg->calculate_pages($count, $limit, $page);	
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'judul' => 'Links',
				'page' => $page,
				'search' => $search,
				'link' => '/admin/tautan/index',
				'pagination' => $pg->Show()
			);
			
			$this->js[] = '/public/backend/controller/delete.js';
			
			$this->render("tautan", 'admin/tautan/index.php', $var);
		}

		public function add(){
			$judul = '';
			$taut = '';
			
			if(!empty($_POST)) {
				$judul = isset($_POST['judul']) ? trim($_POST['judul']) : '';
				$taut = isset($_POST['taut']) ? trim($_POST['taut']) : '';
				
				$validator = new Validator();
				//$validator->addRule('judul', array('require'));
				$validator->addRule('taut', array('require'));			
				
				$setdata = array(
					//'judul' => $judul,
					'taut' => $taut
				);						
				
				
				$validator->setData($setdata);
	
				if($validator->isValid()) {
					
					$data = array(
						'judul' => $judul,
						'taut' => $taut,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->tautan;
					$col->insert($data);
					
					
					$this->redirect('/admin/tautan/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'judul' => $judul,
				'taut' => $taut,
				'link' => '/admin/tautan/add',
				'header' => ' Add link'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';
			$this->js[] = '/public/backend/controller/autosize.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('people', 'admin/tautan/add.php', $var);
		}

		public function edit(){
			$page = $this->getPage();
			$id = $_GET['id'];
			$db = Db::init();
			$col = $db->tautan;
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			
			$error = array();
			$judul = isset($mcol['judul']) ? trim($mcol['judul']) : '';
			$taut = isset($mcol['taut']) ? trim($mcol['taut']) : '';
			
			if(!empty($_POST)) {
				$judul = isset($_POST['judul']) ? trim($_POST['judul']) : '';
				$taut = isset($_POST['taut']) ? trim($_POST['taut']) : '';
				
				$validator = new Validator();
				//$validator->addRule('judul', array('require'));
				$validator->addRule('taut', array('require'));
							
				
				$setdata = array(
					//'judul' => $judul,
					'taut' => $taut
				);	
				
				$validator->setData($setdata);					
				
					if($validator->isValid()) {
					
					
					$date = strtotime($released_date);
					
					$data = array(
						'judul' => $judul,
						'taut' => $taut,
					);
					
					
					$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
					
					
					$this->redirect('/admin/tautan/index?page='.$page);
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'judul' => $judul,
				'taut' => $taut,
				'link' => '/admin/tautan/edit?id='.$id.'&page='.$page,
				'header' => ' Edit link'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('people', 'admin/tautan/add.php', $var);
		}

		public function delete() {
			$page = $this->getPage();
			$id = $_GET['id'];
			
			$db = Db::init();
			$col = $db->tautan;
			
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/tautan/index?page='.$page);
			exit;
		}
	}
?>