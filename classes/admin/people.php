<?php
	class people_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_adminpeople']) ? $_SESSION['search_adminpeople'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_adminpeople'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('job' => $regex), array('namap' => $regex)
				));			
			}
		
			$db = Db::init();
			
			$colpeople = $db->peoples;
			
			$data = $colpeople->find($query)->limit($limit)->skip($skip)->sort(array('time_created' => 1));
			$count = $colpeople->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/people/index?page=';
			$pg->calculate_pages($count, $limit, $page);	
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'judul' => ' Cast and Crew List',
				'page' => $page,
				'search' => $search,
				'link' => '/admin/people/index',
				'pagination' => $pg->Show()
			);
			
			$this->js[] = '/public/backend/controller/delete.js';
			
			$this->render("people", "admin/people/index.php", $var);
		}

		public function add(){
			$job = '';
			$narasi = '';
			$namap = '';
			$namafile = '';
			
			if(!empty($_POST)) {
				$job = isset($_POST['job']) ? trim($_POST['job']) : '';
				$narasi = isset($_POST['narasi']) ? trim($_POST['narasi']) : '';
				$namap = isset($_POST['namap']) ? trim($_POST['namap']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('job', array('require'));
				$validator->addRule('namap', array('require'));			
				$validator->addRule('narasi', array('require'));
				
				$setdata = array(
					'job' => $job,
					'namap' => $namap,
					'narasi' => $narasi
				);						
				
				
				$validator->setData($setdata);
	
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }								
					
					$data = array(
						'job' => $job,
						'namap' => $namap,
						'narasi' => $narasi,
						'foto' => $namafileshafoto,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->peoples;
					$col->insert($data);
					
					if(strlen(trim($namafileshafoto)) > 0) {
						if(! is_dir(IMGPATH.'bidahcinta')) {
							if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
					}
					
					$this->redirect('/admin/people/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				//'error' => $error,
				'job' => $job,
				'narasi' => $narasi,
				'namap' => $namap,
				'namafile' => $namafile,
				'link' => '/admin/people/add',
				'judul' => ' Add People Data'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';
			$this->js[] = '/public/backend/controller/autosize.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('people', 'admin/people/add.php', $var);
		}

		public function edit(){
			$page = $this->getPage();
			$id = $_GET['id'];
			$db = Db::init();
			$col = $db->peoples;
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			
			$error = array();
			$job = isset($mcol['job']) ? trim($mcol['job']) : '';
			$narasi = isset($mcol['narasi']) ? trim($mcol['narasi']) : '';
			$namap = isset($mcol['namap']) ? trim($mcol['namap']) : '';
			$namafile = isset($mcol['foto']) ? trim($mcol['foto']) : '';
			
			if(!empty($_POST)) {
				$job = isset($_POST['job']) ? trim($_POST['job']) : '';
				$narasi = isset($_POST['narasi']) ? trim($_POST['narasi']) : '';
				$namap = isset($_POST['namap']) ? trim($_POST['namap']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('job', array('require'));
				$validator->addRule('namap', array('require'));
				$validator->addRule('narasi', array('require'));
							
				
				$setdata = array(
					'job' => $job,
					'narasi' => $narasi,
					'namap' => $namap
				);	
				
				$validator->setData($setdata);					
				
					if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$date = strtotime($released_date);
					
					$data = array(
						'job' => $job,
						'namap' => $namap,
						'narasi' => $narasi,
						'foto' => $namafileshafoto
					);
					
					
					$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'bidahcinta')) {
								if (!mkdir(IMGPATH.'bidahcinta', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'bidahcinta/'.$namafile)) {
									if(! unlink(IMGPATH.'bidahcinta/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'bidahcinta/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/people/index?page='.$page);
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'job' => $job,
				'narasi' => $narasi,
				'namap' => $namap,
				'namafile' => $namafile,
				'link' => '/admin/people/edit?id='.$id.'&page='.$page,
				'judul' => ' Edit People Data'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('people', 'admin/people/add.php', $var);
		}
		
		public function delete() {
			$page = $this->getPage();
			$id = $_GET['id'];
			
			$db = Db::init();
			$col = $db->peoples;
			
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			if(isset($mcol['foto'])) {
				if(strlen(trim($mcol['foto'])) > 0) {
					if(file_exists(IMGPATH.'bidahcinta/'.$mcol['foto'])) {
						if(! unlink(IMGPATH.'bidahcinta/'.$mcol['foto'])){
							die('Failed to delete file...');
						}
					}
				}				
			}
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/people/index?page='.$page);
			exit;
		}
	}
?>