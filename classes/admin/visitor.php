<?php
	class visitor_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$db = Db::init();
			
			$colvis = $db->visitor;
			
			$data = $colvis->find()->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $colvis->count();
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/visitor/index?page=';
			$pg->calculate_pages($count, $limit, $page);
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'judul' => ' Visitors',
				'total' => $count,
				'page' => $page,
				'link' => '/admin/visitor/index',
				'pagination' => $pg->Show()
			);
			
			$this->js[] = '/public/backend/controller/delete.js';
			
			$this->render("visitor", "admin/visitor/index.php", $var);
			
		}
	}
?>