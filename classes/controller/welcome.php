<?php

	class welcome_controller extends controller{
		
		public function index(){
			//$cekip = $_SERVER['REMOTE_ADDR']."";	
			//helper::getVisitor($cekip);	
			$db = Db::init();
			$colpref = $db->preference;
			$colgallery = $db->gallery;
			
			$datapref = $colpref->findone();
			$dataslider = $colgallery->find(array('slider' => 'yes'))->sort(array('time_created' => 1));
			
			
			$var = array(
				'datapref' => $datapref,
				'dataslider' => $dataslider
			);
			
			$this->render('home', 'welcome/index.php', $var);
			}
	}

?>
