<?php
	class castcrew_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$col = $db->peoples;
			$colimg = $db->gallery;
			
			$datapeople = $col->find()->sort(array('time_created' => 1));
			$datagal = $colimg->find(array('slider' => 'no'))->sort(array('time_created' => 1));
			
			$var = array(
				'data' => $datapeople,
				'dataslider' => $datagal
			);
			
			$this->render("castcrew", "castcrew/index.php", $var);
		}
	}
?>