<?php
	class synopsis_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$col = $db->synopsis;
			$colimg = $db->gallery;
			
			$datasy = $col->findone();
			$datagal = $colimg->find(array('slider' => 'no'))->sort(array('time_created' => 1));
			
			
			$var = array(
				'data' => $datasy,
				'dataslider' => $datagal
			);
			
			$this->render("synopsis", "synopsis/index.php", $var);
		}
	}
?>