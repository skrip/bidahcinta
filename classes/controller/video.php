<?php
	class video_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colimg = $db->gallery;
			$colvid = $db->videos;
			
			$datavid = $colvid->find()->sort(array('time_created' => 1));
			$datagal = $colimg->find(array('slider' => 'no'))->sort(array('time_created' => 1));
			
			$var = array(
				'dataslider' => $datagal,
				'data' => $datavid
			);
			$this->render("video", "video/index.php", $var);
		}
		
	}
?>