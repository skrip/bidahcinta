<?php
	class tautan_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$coltaut = $db->tautan;
			$colimg = $db->gallery;
			
			$datataut = $coltaut->find()->sort(array('time_created' => 1));
			$datagal = $colimg->find(array('slider' => 'no'))->sort(array('time_created' => 1));
			
			$var = array(
				'data' => $datataut,
				'dataslider' => $datagal
			);
			
			$this->render("tautan", 'tautan/index.php', $var);
		}
	}
?>