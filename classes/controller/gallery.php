<?php
	class gallery_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$col = $db->gallery;
			
			$datacol = $col->find(array('slider' => 'no'))->sort(array('time_created' => 1));
			
			$var = array(
				'data' => $datacol
			);
			
			$this->render("gallery", "gallery/index.php", $var);
		}
	}
?>