$(document).ready(function($){
	$("#deleteDialog").dialog({
	        autoOpen: false,
	        modal: true,
	        width: "440",
	        buttons: [{
	                text: "Delete",
	                click: function() {
	                    $( this ).dialog( "close" );
	                    var id = $(this).data('link');                                                                  
	                    $(location).attr('href', id);
	                }
	            },
	            {
	                text: "Cancel",
	                click: function() {
	                    $( this ).dialog( "close" );
	                }
	        }]
	    });	    
	    $(".delete").live("click", function (event) {
	    	var ss = "Anda yakin akan menghapus data <b>" + $(this).attr("data-controller") + " <i>" + $(this).attr("data-name") + "</i></b> !!!";
            $("#deleteDialog p").html(ss);
	        $("#deleteDialog").dialog("option", { modal: false, title: "Hapus Data " + $(this).attr("data-controller") })
	        .data("link", $(this).attr("link"))
	        .dialog("open");
	        event.preventDefault();
	    });
});
