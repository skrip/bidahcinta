$(document).ready(function() {
	
	$(document).on('click', '.ck-item', function() {
		var si = $('#selected-item').val();
		var p = $(this).attr('data-pos');
		var cek = $(this).is(':checked');
		var id = $(this).attr('data-id');
		var oldid = '';	   			
		
		if(cek) {
			$('#selected-item').val(p);
			if(si != p) {
				$('#item-'+si).prop('checked', false);
				oldid = $('#item-'+si).attr('data-id');
			}					   			
		}	   				
		else {
			if(si != p) {
				$('#item-'+si).prop('checked', false);
				oldid = $('#item-'+si).attr('data-id');
			}
			else
				oldid = $('#item-'+p).attr('data-id');				
				
			$('#selected-item').val('');   					
		}   					
		
		$.post('/util/setDefaultContent', {'id': id, 'old': oldid}, function(data) {
			
		});
	});
    
	$('body').delegate('.btn-add-language', 'click', function() {
		$.get('/util/addContent', function(data) {
			$('#view-content').replaceWith(data);
			$("select.mws-select2").select2();
			$('textarea.content').cleditor({ width: '100%' });			
		});							
	});
		
	$('textarea.content').cleditor({ width: '100%' });
	$("select.mws-select2").select2();
});
