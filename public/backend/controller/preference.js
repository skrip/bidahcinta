$(document).ready(function(){
	
	$('body').delegate('.use', 'click', function() {
		var v = $(this).val();
		if(v == 'yes') {
			$.get('/util/addDefaultProduct',function(data) {
				$('#view-highlight-product').replaceWith(data);
				$("select.mws-select2").select2();
			});
		}
		else {
			$('#view-highlight-product').html('');
		}		
	});
	
	$('body').delegate('.btn-add-language', 'click', function() {
		$.get('/util/addIntro', function(data) {
			$('#view-content').replaceWith(data);
			$("select.mws-select2").select2();
			$('textarea.content').cleditor({ width: '100%' });			
		});							
	});
		
	if( $.fn.select2 ) {
        $("select.mws-select2").select2();
    }
    $('textarea.content').cleditor({ width: '100%' });
	$('textarea.description').cleditor({ width: '100%' });	
});
