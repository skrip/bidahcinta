$(document).ready(function() {
    
	$('body').delegate('.btn-add-language', 'click', function() {
		$.get('/util/addMenu', function(data) {
			$('#view-menu').replaceWith(data);
			$("select.mws-select2").select2();		
		});							
	});
	
	$("select.mws-select2").select2();
});
