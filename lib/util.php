<?php

class util_controller
{
	public static function passwordHash($pwd)
	{
		$salt = 'POSokoklayau123456_';
		$salt = md5($salt.$pwd);
				
		$options = [
			'cost' => 11,
			'salt' => $salt,
		];
		$pwd = password_hash($pwd, PASSWORD_BCRYPT, $options);
		return $pwd;
	}
	
	public function getrelease() {
		$s = '<div class="mws-form-row">';
		$s .= '<div class="mws-form-cols">';
		$s .= '<label class="mws-form-label">Released Date </label>';    				
	    $s .= '<div class="mws-form-col-2-8">';
    	$s .= '<input type="text" name="released_date" class="mws-datepicker small" value="'.date('d-m-Y', time()).'">';
        $s .= '</div>';    					
		$s .= '</div>';
		$s .= '</div>';
		
		echo $s;
	}
	
	public function getondate() {
		$s = '<div class="mws-form-row">';
		$s .= '<div class="mws-form-cols">';
		$s .= '<label class="mws-form-label">Coming Soon on Date <span class="required">*</span></label>';    				
	    $s .= '<div class="mws-form-col-2-8">';
    	$s .= '<input type="text" name="ondate" class="mws-datepicker small" value="'.date('d-m-Y', time()).'">';
        $s .= '</div>';    					
		$s .= '</div>';
		$s .= '</div>';
		
		echo $s;
	}
	
	public function getonmonth() {
		$m = array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'				
		);
		
		$mm = date('m', time());
		$yy = date('Y', time());
		
		$s = '<div class="mws-form-row">';
    	$s .= '<div class="mws-form-cols">';
    	$s .= '<label class="mws-form-label">Coming Soon on Month <span class="required">*</span></label>';
	    $s .= '<div class="mws-form-col-2-8">';
		$s .= '<select style="width:100%" name="month" class="mws-select2 small" data-placeholder="Select a Month...">';
		$s .= '<option value="" ></option>';
		foreach($m as $k=>$v)
			if($k==$mm)
				$s .= '<option value="'.$k.'" selected>'.$v.'</option>';
			else
				$s .= '<option value="'.$k.'" >'.$v.'</option>';
		$s .= '</select>';		    						    				
		$s .= '</div>';
		$s .= '<div class="mws-form-col-2-8">';
		$s .= '<select name="year" class="mws-select2 small" data-placeholder="Select a Year...">';
		$s .= '<option value="" ></option>';
		for ($i=2016; $i < 2027; $i++)
			if($i==$yy)
				$s .= '<option value="'.$i.'" selected>'.$i.'</option>';
			else
				$s .= '<option value="'.$i.'" >'.$i.'</option>';
		$s .= '</select>';
		$s .= '</div>';
		$s .= '</div>';
		$s .= '</div>';
		
		echo $s;
	}
}
