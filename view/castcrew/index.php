<div id="ajax-content">
	<!--PAGE INFO FOR UPDATING BODY CLASS AND DOC TITLE-->
	<div id="page-info" data-page-title="Cast &amp; Crew | VYSUAL - In Select Theaters December 25" class="archive category ajax-on"></div>
	
	<div class="posts-container">	
		<div id="slide-right" class="slide-nav"><span></span></div>
		<div id="slide-left" class="slide-nav"><span></span></div>
	
		<h1 class="entrytitle">Cast &amp; Crew</h1>
	
		<div class="scroll-wrapper">
			<div class="scroll-this">
				<?php
					foreach ($data as $dt) {
						echo '<div class="post">
								<div class="image-container">
									<img src="/showfile/show?namafile='.$dt['foto'].'" alt="" />
								</div>			
								<div class="postInfo">		
									<h2 class="posttitle">'.$dt['namap'].' <br /> '.$dt['job'].'</h2>
									<p>'.$dt['narasi'].'</p>
								</div>
							  </div>';
					}
				?>
				<!--end post-->
		
			</div><!--end scroll-this-->
		</div><!--end scroll-wrapper-->
	</div><!--end posts-container-->
	
	<!--IMAGE SLIDESHOW-->
	<div id="headerImages">
		<?php
			foreach ($dataslider as $key) {
				echo '<div class="header-image" style="background-image:url(/showfile/show?namafile='.$key['filename'].');"><img src="/showfile/show?namafile='.$key['filename'].'" alt=""></div>';
			}
		?>
	</div>
	<!--end headerImages-->
	
	<!--VIDEO HOLDER-->
	<div class="videoContainer">
		<div class="closeVideo">&times;</div>
	</div>
		
</div>