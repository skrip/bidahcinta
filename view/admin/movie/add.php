<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<?php if(strlen(trim($namafile)) > 0) {?>
    			<div class="mws-form-row">
    				<label class="mws-form-label"></label>
    				<div class="mws-form-item">
    					<img width="500" src="/showfile/show?namafile=<?php echo $namafile; ?>" />
    				</div>
    			</div>
    			<?php }?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">Poster</label>    				
                        <div class="mws-form-col-3-8">
                        	<input type="file" name="foto" class="photo" />
                        	<div class="mws-error">
                        	<?php    						
		                	if(isset($error['width']))
							{
								foreach($error['width'] as $message)
									echo $message;
							}									                	
		                	?>
		                	</div>
                        </div>    					
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Title <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['title']) ? ' error' : ''; ?>
    					<input type="text" name="title" value="<?php echo $title; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['title']))
						{
							echo '<div class="mws-error">';
							foreach($error['title'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Genre <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['genre']) ? ' error' : ''; ?>
    					<input type="text" name="genre" value="<?php echo $genre; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['genre']))
						{
							echo '<div class="mws-error">';
							foreach($error['genre'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Producer</label>
    				<div class="mws-form-item">
    					<input type="text" name="producer" value="<?php echo $producer; ?>" class="small tags" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Writer</label>
    				<div class="mws-form-item">
    					<input type="text" name="writer" value="<?php echo $writer; ?>" class="small tags" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Director</label>
    				<div class="mws-form-item">
    					<input type="text" name="director" value="<?php echo $director; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Cinematographer</label>
    				<div class="mws-form-item">
    					<input type="text" name="cinematographer" value="<?php echo $cinematographer; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Cast</label>
    				<div class="mws-form-item">
    					<input type="text" name="cast" value="<?php echo $cast; ?>" class="small tags" />
    				</div>
    			</div>    			
    			<div class="mws-form-row">
    				<label class="mws-form-label">Status</label>
    				<div class="mws-form-item clearfix">
    					<ul class="mws-form-list inline">
    						<li><input id="release" class="use" type="radio" name="status" value="release" <?php echo ($status == 'release') ? 'checked="checked"' : ''; ?>> <label>Now Showing</label></li>
							<li><input class="use" type="radio" name="status" value="release_in_cinema" <?php echo ($status == 'release_in_cinema') ? 'checked="checked"' : ''; ?>> <label>Now Showing in Cinema</label></li>
							<li><input id="on_date" class="use" type="radio" name="status" value="coming_soon_on_date" <?php echo ($status == 'coming_soon_on_date') ? 'checked="checked"' : ''; ?>> <label>Coming Soon on Date</label></li>
							<li><input id="on_month" class="use" type="radio" name="status" value="coming_soon_on_month" <?php echo ($status == 'coming_soon_on_month') ? 'checked="checked"' : ''; ?>> <label>Coming Soon on Month</label></li>
							<li><input class="use" type="radio" name="status" value="coming_soon" <?php echo ($status == 'coming_soon') ? 'checked="checked"' : ''; ?>> <label>Coming Soon</label></li>
    					</ul>
    				</div>
    			</div>
    			<div id="vrelease">
				<?php if($status == 'release') { ?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">Released Date</label>    				
                        <div class="mws-form-col-2-8">
                        	<?php
		                	if(isset($error['released_date']))
							{
								echo '<input type="text" name="released_date" class="mws-datepicker small error" value="'.$released_date.'">';
								echo '<div class="mws-error">';
								foreach($error['released_date'] as $message)
									echo $message;
								echo '</div>';
							}	
							else
								echo '<input type="text" name="released_date" class="mws-datepicker small" value="'.$released_date.'">';									                	
		                	?>
                        </div>    					
    				</div>
    			</div>
    			<?php } ?>
    			</div>
    			<div id="cs_ondate">
    			<?php if($status == 'coming_soon_on_date') { ?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">Coming Soon on Date <span class="required">*</span></label>    				
                        <div class="mws-form-col-2-8">
                        	<?php
		                	if(isset($error['ondate']))
							{
								echo '<input type="text" name="ondate" class="mws-datepicker small error" value="'.$ondate.'">';
								echo '<div class="mws-error">';
								foreach($error['ondate'] as $message)
									echo $message;
								echo '</div>';
							}	
							else
								echo '<input type="text" name="ondate" class="mws-datepicker small" value="'.$ondate.'">';									                	
		                	?>
                        </div>    					
    				</div>
    			</div>
    			<?php } ?>
    			</div>
    			<?php
    			$m = array(
    				'01' => 'January',
    				'02' => 'February',
    				'03' => 'March',
    				'04' => 'April',
    				'05' => 'May',
    				'06' => 'June',
    				'07' => 'July',
    				'08' => 'August',
    				'09' => 'September',
    				'10' => 'October',
    				'11' => 'November',
    				'12' => 'December'				
				);
    			?>
    			<div id="cs_onmonth">
    			<?php if($status == 'coming_soon_on_month') { ?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">Coming Soon on Month <span class="required">*</span></label>
	    				<div class="mws-form-col-2-8">
		    				<select style="width:100%" name="month" class="mws-select2 small" data-placeholder="Select a Month...">
		    					<option value="" ></option>	    					
		    					<?php
									foreach($m as $k=>$v) {
										if($k == $month)
											echo '<option value="'.$k.'" selected>'.$v.'</option>';
										else
											echo '<option value="'.$k.'" >'.$v.'</option>';									
									}																    					
		    					?>
		    				</select>		    						    				
						</div>
						<div class="mws-form-col-2-8">
							<select name="year" class="mws-select2 small" data-placeholder="Select a Year...">
		    					<option value="" ></option>	    					
		    					<?php
		    						for ($i=2016; $i < 2027; $i++) { 
										if($i == $year)
											echo '<option value="'.$i.'" selected>'.$i.'</option>';
										else
											echo '<option value="'.$i.'" >'.$i.'</option>';
									}															    					
		    					?>
		    				</select>
						</div>
						<?php    						
		                	if(isset($error['onmonth']))
							{
								echo '<div class="mws-error">';
								foreach($error['onmonth'] as $message)
									echo $message;
								echo '</div>';
							}									                	
		                	?>
					</div>
    			</div> 
    			<?php } ?>  
    			</div> 			
    			<div class="mws-form-row">
    				<label class="mws-form-label">Synopsis </label>
    				<div class="mws-form-item clearfix">
    					<textarea name="synopsis" id="content" rows="auto" cols="auto" class="content large"><?php echo $synopsis; ?></textarea>   					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>