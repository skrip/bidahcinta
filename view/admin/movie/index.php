<?php
	$db = Db::init();
?>
<div class="mws-panel grid_8">
	<h2><?php echo $judul; ?></h2>
</div>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="pull-left"><i class="icon-table"></i> <?php echo $judul; ?></span>
    </div>
    <div class="mws-panel-toolbar">
    	<div class="btn-toolbar">
    		<div class="btn-group">
    			<a href="/admin/movie/add" class="btn btn-primary small" id="mws-themer-getcss"><i class="icon-plus"></i> Add New Data</a>
    		</div>
    	</div>
    </div>
    <div class="mws-panel-body no-padding">
    	<div class="dataTables_wrapper">
	    	<div class="dataTables_filter">
				<form method="post" action="<?php echo $link;?>">				
					<label><input type="text" placeholder="Search..." name="search" value="<?php echo $search;?>"></label>
					<button type="submit" class="btn"><i class="icol-find"></i></button>
				</form>
			</div>
	        <table class="mws-table">
	            <thead>
	                <tr>
	                	<th></th>
	                	<th>Title</th>
	                    <th>Genre</th>
						<th>Released Date</th>
						<th>Status</th>
						<th>Action</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            foreach($data as $dat) {
	            	echo '<tr>';
					if(isset($dat['poster'])) {
						if(strlen(trim($dat['poster'])) > 0)
							echo '<td width="80"><img width="80" src="/showfile/show?namafile='.$dat['poster'].'" alt=""/></td>';
						else
							echo '<td width="80"></td>';
					}
					else
						echo '<td width="80"></td>';
					echo '<td>'.ucwords($dat['title']).'</td>';
					echo '<td>'.ucwords($dat['genre']).'</td>';
					echo '<td>'.date('d F Y', $dat['released_date']).'</td>';
					echo '<td>'.ucwords($dat['status']).'</td>';
					echo '<td width="120">';
					echo '<span class="btn-group">';
			        echo '<a href="/admin/movie/edit?id='.$dat['_id'].'&page='.$page.'" class="btn btn-small" rel="tooltip" data-placement="top" title="Edit Gallery"><i class="icol-pencil"></i></a> ';
			        echo '<a href="#" link="/admin/movie/delete?id='.$dat['_id'].'&page='.$page.'" rel="tooltip" data-placement="top" title="Delete Movie" class="btn btn-small delete" data-controller="movie" data-name="'.$dat['title'].'"><i class="icol-cancel"></i></a>';
			        echo '</span>';
			        echo '</td>';
					echo '</tr>';
	            }
	            ?>
	            </tbody>
	        </table>
	        <?php echo $pagination;?>
	    </div>
	</div>
</div>
<?php echo helper::showDialog(); ?>