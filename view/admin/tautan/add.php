<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $header;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">TITLE <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['judul']) ? ' error' : ''; ?>
    					<input type="text" name="judul" value="<?php echo $judul; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['judul']))
						{
							echo '<div class="mws-error">';
							foreach($error['judul'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">LINK <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['taut']) ? ' error' : ''; ?>
    					<input type="text" name="taut" value="<?php echo $taut; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['taut']))
						{
							echo '<div class="mws-error">';
							foreach($error['taut'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    		</div>
    		<div class="mws-button-row">
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>