<?php
	$db = Db::init();
?>
<div class="mws-panel grid_8">
	<h2><?php echo $judul; ?></h2>
</div>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="pull-left"><i class="icon-table"></i> <?php echo $judul; ?></span>
    </div>
    <div class="mws-panel-toolbar">
    	<div class="btn-toolbar">
    		<div class="btn-group">
    			<a href="/admin/people/add" class="btn btn-primary small" id="mws-themer-getcss"><i class="icon-plus"></i> Add New Data</a>
    		</div>
    	</div>
    </div>
    <div class="mws-panel-body no-padding">
    	<div class="dataTables_wrapper">
	    	<div class="dataTables_filter">
				<form method="post" action="<?php echo $link;?>">				
					<label><input type="text" placeholder="Search by job or name" name="search" value="<?php echo $search;?>"></label>
					<button type="submit" class="btn"><i class="icol-find"></i></button>
				</form>
			</div>
	        <table class="mws-table">
	            <thead>
	                <tr>
	                	<th></th>
	                	<th>JOB</th>
	                	<th>NAMA</th>
	                    <th>NARASI</th>
	                    <th>ACTION</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            foreach($data as $dat) {
	            	echo '<tr>';
					if(isset($dat['foto'])) {
						if(strlen(trim($dat['foto'])) > 0)
							echo '<td width="80"><img width="80" src="/showfile/show?namafile='.$dat['foto'].'" alt=""/></td>';
						else
							echo '<td width="80"></td>';
					}
					else
					echo '<td width="80"></td>';
					echo '<td>'.ucwords($dat['job']).'</td>';
					echo '<td>'.ucwords($dat['namap']).'</td>';
					echo '<td>'.ucwords($dat['narasi']).'</td>';
					echo '<td width="120">';
					echo '<span class="btn-group">';
			        echo '<a href="/admin/people/edit?id='.$dat['_id'].'&page='.$page.'" class="btn btn-small" rel="tooltip" data-placement="top" title="Edit People"><i class="icol-pencil"></i></a> ';
			        echo '<a href="#" link="/admin/people/delete?id='.$dat['_id'].'&page='.$page.'" rel="tooltip" data-placement="top" title="Delete People" class="btn btn-small delete" data-controller="people" data-name="'.$dat['job'].'"><i class="icol-cancel"></i></a>';
			        echo '</span>';
			        echo '</td>';
					echo '</tr>';
	            }
	            ?>
	            </tbody>
	        </table>
	        <?php echo $pagination;?>
	    </div>
	</div>
</div>
<?php echo helper::showDialog(); ?>