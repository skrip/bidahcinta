<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<?php if(strlen(trim($namafile)) > 0) {?>
    			<div class="mws-form-row">
    				<label class="mws-form-label"></label>
    				<div class="mws-form-item">
    					<img width="500" src="/showfile/show?namafile=<?php echo $namafile; ?>" />
    				</div>
    			</div>
    			<?php }?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">foto</label>    				
                        <div class="mws-form-col-3-8">
                        	<input type="file" name="foto" class="photo" />
                        	<div class="mws-error">
                        	<?php    						
		                	if(isset($error['width']))
							{
								foreach($error['width'] as $message)
									echo $message;
							}									                	
		                	?>
		                	</div>
                        </div>    					
    				</div>
    			</div>
    			<!--
				<div class="mws-form-row">
									<label class="mws-form-label">JOB <span class="required">*</span></label>
									<div class="mws-form-item">
										<select name="job">
											<option value="director">Director</option>
											<option value="screenwriter">Screenwriter</option>
											<option value="producer">Producer</option>
											<option value="cast">Cast</option>
										</select>
										<label style="color: red;">Setiap melakukan edit data, bagian ini jangan lupa disesuaikan ya.</label>
									</div>
								</div>-->
				<div class="mws-form-row">
    				<label class="mws-form-label">JOB <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['job']) ? ' error' : ''; ?>
    					<input type="text" name="job" value="<?php echo $job; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['job']))
						{
							echo '<div class="mws-error">';
							foreach($error['job'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Nama <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['namap']) ? ' error' : ''; ?>
    					<input type="text" name="namap" value="<?php echo $namap; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['namap']))
						{
							echo '<div class="mws-error">';
							foreach($error['namap'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Narasi <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['narasi']) ? ' error' : ''; ?>
    					<input type="text" name="narasi" value="<?php echo $narasi; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['narasi']))
						{
							echo '<div class="mws-error">';
							foreach($error['narasi'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    		</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>