<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<?php if(strlen(trim($namafile)) > 0) {?>
    			<div class="mws-form-row">
    				<label class="mws-form-label"></label>
    				<div class="mws-form-item">
    					<img width="500" src="/showfile/show?namafile=<?php echo $namafile; ?>" />
    				</div>
    			</div>
    			<?php }?>
    			<div class="mws-form-row">
    				<div class="mws-form-cols">
    					<label class="mws-form-label">Image <span class="required">*</span></label>    				
                        <div class="mws-form-col-3-8">
                        	<input type="file" name="foto" class="photo" />
                        	<div class="mws-error">
                        	<?php    						
		                	if(isset($error['width']))
							{
								foreach($error['width'] as $message)
									echo $message;
							}									                	
		                	?>
		                	</div>
                        </div>    					
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Title</label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['title']) ? ' error' : ''; ?>
    					<input type="text" name="title" value="<?php echo $title; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['title']))
						{
							echo '<div class="mws-error">';
							foreach($error['title'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Description </label>
    				<div class="mws-form-item clearfix">
    					<textarea name="description" id="content" rows="auto" cols="auto" class="content large"><?php echo $description; ?></textarea>   					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>