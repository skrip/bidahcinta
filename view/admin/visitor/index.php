<?php
$db = Db::init();
?>
<div class="mws-panel grid_8">
	<h2><?php echo $judul; ?></h2>
</div>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="pull-left"><i class="icon-table"></i> <?php echo $judul; ?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<div class="dataTables_wrapper">
	    	<div class="dataTables_filter">
				<form method="" action="">				
					<label>Total Visitor: <input type="text" name="total" value="<?php echo $total;?>" readonly></label>
				</form>
			</div>
	        <table class="mws-table">
	            <thead>
	                <tr>
	                    <th>IP Address</th>
	                    <th>Kota</th>
						<th>Negara</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            foreach($data as $dat) {
	            	echo '<tr>';
					echo '<td> '.ucwords($dat['ipaddress']).' </td>';
					echo '<td> '.ucwords($dat['city']).' </td>';
					echo '<td>'.ucwords($dat['country']).'</td>';
					echo '</tr>';
	            }
	            ?>
	            </tbody>
	        </table>
	        <?php echo $pagination;?>
	    </div>
	</div>
</div>
<?php echo helper::showDialog(); ?>