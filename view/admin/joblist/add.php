<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">JOB NAME</label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['job']) ? ' error' : ''; ?>
    					<input type="text" name="job" value="<?php echo $job; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['job']))
						{
							echo '<div class="mws-error">';
							foreach($error['job'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Nama Person <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['namap']) ? ' error' : ''; ?>
    					<input type="text" name="namap" value="<?php echo $namap; ?>" class="small <?php echo $cls; ?> small tags" />
    					<?php    						
	                	if(isset($error['namap']))
						{
							echo '<div class="mws-error">';
							foreach($error['namap'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    		</div>
    		<div class="mws-button-row">
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>