<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">Synopsis</label>
    				<div class="mws-form-item clearfix">
    					<textarea name="narasi" id="content" rows="auto" cols="auto" class="content large"><?php echo $narasi; ?></textarea>    					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>