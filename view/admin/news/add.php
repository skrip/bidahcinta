<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">Title <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['title']) ? ' error' : ''; ?>
    					<input type="text" name="title" value="<?php echo $title; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['title']))
						{
							echo '<div class="mws-error">';
							foreach($error['title'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Shown As</label>
    				<div class="mws-form-item clearfix">
    					<ul class="mws-form-list inline">
    						<?php
    						if($shownas == 'highlight') {
    							echo '<li><input class="use" type="radio" name="shownas" value="highlight" checked> <label>Short Content</label></li>
    							<li><input class="use" type="radio" name="shownas" value="content"> <label>Content</label></li>';
    						}
							else {
								echo '<li><input class="use" type="radio" name="shownas" value="highlight" > <label>Short Content</label></li>
    							<li><input class="use" type="radio" name="shownas" value="content" checked> <label>Content</label></li>';
							}							
    						?>
    					</ul>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Short Description</label>
    				<div class="mws-form-item clearfix">
    					<textarea name="highlight" rows="auto" cols="auto" class="small autosize"><?php echo $highlight; ?></textarea>    					
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Content</label>
    				<div class="mws-form-item clearfix">
    					<textarea name="content" id="content" rows="auto" cols="auto" class="content large"><?php echo $content; ?></textarea>
    					<?php    						
	                	if(isset($error['content']))
						{
							echo '<div class="mws-error">';
							foreach($error['content'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>    					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>