<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">Title <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<input type="text" name="title" value="<?php echo $title; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Description </label>
    				<div class="mws-form-item clearfix">
    					<textarea name="description" id="content" rows="auto" cols="auto" class="content large"><?php echo $description; ?></textarea>   					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>