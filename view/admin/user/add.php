<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">Name <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php    						
	                	if(isset($error['name']))
						{
							echo '<input type="text" name="name" value="'.$name.'" class="error small" />';
							echo '<div class="mws-error">';
							foreach($error['name'] as $message)
								echo $message;
							echo '</div>';
						}	
						else
							echo '<input type="text" name="name" value="'.$name.'" class="small" />';									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Username <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php    						
	                	if(isset($error['email']))
						{
							echo '<input type="text" name="username" value="'.$username.'" class="error small" />';
							echo '<div class="mws-error">';
							foreach($error['username'] as $message)
								echo $message;
							echo '</div>';
						}	
						else
							echo '<input type="text" name="username" value="'.$username.'" class="small" />';									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Password <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php    						
	                	if(isset($error['password']))
						{
							echo '<input type="password" name="pass" value="'.$pass.'" class="error small" />';
							echo '<div class="mws-error">';
							foreach($error['password'] as $message)
								echo $message;
							echo '</div>';
						}	
						else
							echo '<input type="password" name="pass" value="'.$pass.'" class="small" />';									                	
	                	?>
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="submit" value="Submit" class="btn btn-danger">
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>