<div id="ajax-content">
	<!--PAGE INFO FOR UPDATING BODY CLASS AND DOC TITLE-->
	<div id="page-info" data-page-title="Gallery | VYSUAL - In Select Theaters December 25" class="page page-template-page-gallery page-template-page-gallery-php ajax-on"></div>

	<ul id="attachmentGallery">
	<?php
		foreach ($data as $dt) {
			echo '<li class="gallery-image" style="background-image:url(/showfile/show?namafile='.$dt['filename'].');" data-imgtitle="'.$dt['title'].'" data-caption="<p>'.$dt['description'].'</p>">
						<img src="/showfile/show?namafile='.$dt['filename'].'" alt="" />
				  </li>';
		}
	?>
	</ul>
	
	<div id="nextImg" class="gallery-nav"><span></span></div>
	<div id="prevImg" class="gallery-nav"><span></span></div>
	<div id="imgInfo"></div>
</div>