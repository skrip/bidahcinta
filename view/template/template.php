<!DOCTYPE html>
<html lang="en-US">
<head>
<meta name="viewport" content="initial-scale=1.0,width=device-width,maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!--PAGE TITLE-->
<title>Bid'ah Cinta</title>

<!--GOOGLE FONT CSS-->
 <?php
	foreach($font as $datfont)
	{
		echo '<link rel="stylesheet" href="'.$datfont.'">';
	}
?>
<!--LOCAL CSS-->
<?php

	foreach($css as $datcss)
	{
		echo '<link rel="stylesheet" href="'.$datcss.'">';
	}
?>

<!--SITE URL SETTING FOR AJAX REQUESTS-->
<script> var siteUrl = 'http://themolitor.com/vysual_html'; </script>

</head>

<body class="home page page-template-page-reviews ajax-on">

<div id="contentContainer">
	
	<!--LOGO-->
	<?php
		$db = Db::init();
		$colpref = $db->preference;
		$coljoblist = $db->joblist;
		
		$datapref = $colpref->findone();
		$datajob = $coljoblist->find()->sort(array('time_created' => 1));
	?>

	<a id="logo" href="#"><img src="<?php echo '/showfile/show?namafile='.$datapref['logo'].'' ;?>" alt="logo" /></a><!--end logo-->
		
	<!--MENU CONTROL-->
	<div id="menu-control"><span></span></div>
	
		<?php echo $content; ?>
	
	<!--FULL SEARCH BAR-->
	<form role="search" method="get" id="full-search" action="http://themes.themolitor.com/vysual/">
		<input type="text" value="Search Site..." onfocus="this.value='';" name="s" id="big-input" />
	</form>
		
	<!--AUDIO PLAYER-->
	<div id="audioControl" class="paused">
		<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
	</div>
	<audio id="audioPlayer" class="songs">
	  <source src="/public/audio/pelangi.wav" type="audio/wav">
	</audio>
			
	<!--SOCIAL ICONS-->
	<div id="socialIcons">
		<a target="_blank" id="link-instagram" data-title="Instagram" href="https://www.instagram.com/kaningapictures/"><i class="fa fa-instagram"></i></a>
		<a target="_blank" id="link-twitter" data-title="Twitter" href="https://twitter.com/kaningapictures"><i class="fa fa-twitter"></i></a>
		<a target="_blank" id="link-facebook" data-title="Facebook" href="https://www.facebook.com/kaningapictures"><i class="fa fa-facebook"></i></a>		
		<!--
		<a id="link-search" rel="search" data-title="Search" href="#"><i class="fa fa-search"></i></a>		
		<a target="_blank" id="link-rss" data-title="RSS" href="#"><i class="fa fa-rss"></i></a>		
		<a target="_blank" id="link-youtube" data-title="YouTube" href="#"><i class="fa fa-youtube-play"></i></a>		
		<a target="_blank" id="link-vimeo" data-title="Vimeo" href="#"><i class="fa fa-vimeo-square"></i></a>		
		<a target="_blank" id="link-google-plus" data-title="Google+" href="#"><i class="fa fa-google-plus"></i></a>		
		<a target="_blank" id="link-tumblr" data-title="Tumblr" href="#"><i class="fa fa-tumblr"></i></a>		
		<a target="_blank" id="link-flickr" data-title="Flickr" href="#"><i class="fa fa-flickr"></i></a>-->
	</div>
		
	<!--IMAGE SLIDESHOW-->
	<!--
	<div id="headerImages">
		<div class="header-image activeBg" style="background-image:url(/public/images/dock.jpg);"><img src="/public/images/dock.jpg" alt=""></div>
		<div class="header-image" style="background-image:url(/public/images/sunshine.jpg);"><img src="/public/images/sunshine.jpg" alt=""></div>
		<div class="header-image" style="background-image:url(/public/images/hooded.jpg);"><img src="/public/images/hooded.jpg" alt=""></div>
		<div class="header-image" style="background-image:url(/public/images/road.jpg);"><img src="/public/images/road.jpg" alt=""></div>
		<div class="header-image" style="background-image:url(/public/images/woods.jpg);"><img src="/public/images/woods.jpg" alt=""></div>
		<div class="header-image" style="background-image:url(/public/images/foggy.jpg);"><img src="/public/images/foggy.jpg" alt=""></div>
	</div>-->
	<!--end headerImages-->
	
	<!--LOADING ANIMATION-->
	<div id="loading-page"></div>
	
	<!--CONTENT COVER-->
	<div id="contentCover"></div>
	
</div><!--end contentContainer-->

<div id="footerContainer" style="background-color: #5B4025;">
	<div id="footer">  		
		
		<!--CREDITS-->
		<div id="vys-credits">
			
			<!--FOOTER LOGO-->
			<a id="footer-logo" href="/"><img src="<?php echo '/showfile/show?namafile='.$datapref['logo3'].'' ;?>" alt="" /></a>
			
			<!--CREDITS TEXT-->
			<span>KANINGA PICTURES <small>presents</small></span>
			<?php 
				foreach ($datajob as $dtj) {
					$ganti = " ";
					$rejob = str_replace(",", $ganti, $dtj['namap']);
					echo '<span><small> '.$dtj['job'].' </small>'.$rejob.'</span>';
				}
			?>
			<!--
			<span>STUDIO NAME PICTURES <small>presents</small></span>
			<span><small>an</small> PRODUCERS <small>production</small></span>
			<span><small>a</small> JOHN DOE <small>picture</small></span> 
			<span>BOB SMITH</span> 
			<span>JESSICA PHILLIPS</span> 
			<span>"VYSUAL"</span> 
			<span><small>casting by</small> THE CASTING COMPANY</span> 
			<span><small>costumes by</small> THE COSTUME PEOPLE</span> 
			<span><small>production designer</small> THE SET DESIGN CREW</span> 
			<span><small>director of photography</small> CINEMATOGRAPHER</span> 
			<span><small>music by</small> THE COMPOSER DUDE</span> 
			<span><small>edited by</small> THE EDITOR GUY</span> 
			<span><small>executive producer</small> GUY ONE  GUY TWO</span> 
			<span><small>produced by</small> THE GUY WITH ALL THE MONEY</span> 
			<span><small>story by</small> THE NOVELIST</span> 
			<span><small>screenplay by</small> THE SCREEN WRITER</span> 
			<span><small>directed by</small> DIRECTOR NAME HERE</span>-->
			<!--PG-13 RATING-->
			<!--
			<div id="rating" class="pg13-rating">
				<div id="rating-letter">PG-13</div>
				<div id="rating-title">Parents Strongly Cautioned</div>
				<div id="rating-info">Some material may be inappropriate for children under 13</div>
			</div>-->
		</div><!--end vys-credits-->
			
		<!--FOOTER MENU-->
		<div id="footerMenuContainer" class="menu-footer-container">
			<ul id="footerMenu" class="menu">
				<li><a target="_blank" href="/">Film Ratings</a></li>
				<li><a target="_blank" href="/">Parental Guide</a></li>
				<li><a target="_blank" href="/">MPAA</a></li>
				<li><a target="_blank" href="/">Privacy Policy</a></li>
				<li><a target="_blank" href="/">Terms of Use</a></li>
			</ul>
		</div>		
		
		<!--COPYRIGHT NOTICE-->
		<div id="copyright">&copy; Kaninga Pictures <a target="_blank">2016</a></div>
		
	</div><!--end footer-->
</div><!--end footerContainer-->

<!--<div id="sidebar">-->
	<!--SIDEBAR LOGO-->
	<!--<a id="sidebar-logo" href="http://themolitor.com/vysual_html/index.html"><img src="/public/images/logo.png" alt="VYSUAL - In Select Theaters December 25" /></a>-->
	
	<!--MAIN MENU-->
	<!--
	<nav id="navigation" class="hasLogo">
		<ul id="dropmenu" class="menu">
			<li class="current-menu-item"><a href="http://themolitor.com/vysual_html/index.html">Home</a></li>
			<li><a href="http://themolitor.com/vysual_html/videos_page_1.html">Videos 1</a></li>
			<li><a href="http://themolitor.com/vysual_html/videos_list.html">Videos 2</a></li>
			<li><a href="http://themolitor.com/vysual_html/gallery.html">Gallery</a></li>
			<li><a href="http://themolitor.com/vysual_html/page.html">Synopsis</a></li>
			<li><a href="http://themolitor.com/vysual_html/aside_category.html">Cast &#038; Crew</a></li>
			<li><a href="http://themolitor.com/vysual_html/post_category.html">News</a></li>
			<li><a target="_blank" href="http://themolitor.com/vysual">Download</a></li>
		</ul>
	</nav>-->
	<?php echo $menuatas; ?>
	
	<!--WIDGET-->
	<!--
	<ul id="sidebar-widgets">
		<li class="widget">
			<h2 class="widget-title">What is this?</h2>			
			<div class="textwidget">You are viewing the sidebar panel. It's an extra space where you can add as much (or as little) content as you like.</div>
		</li>
	</ul>
	
	</div>-->
	<!--end sidebar-->

<!--JavaScript-->
 <?php
		foreach($js as $datjs)
		{
			echo '<script src="'.$datjs.'"></script>';
		}
		?>


</body>
</html>