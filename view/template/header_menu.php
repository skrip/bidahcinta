<?php
	$db = Db::init();
	$colpref = $db->preference;
	
	$datapref = $colpref->findone();
?>

<div id="sidebar">
	<!--SIDEBAR LOGO-->
	<a id="sidebar-logo" href="/"><img src="<?php echo '/showfile/show?namafile='.$datapref['logo2'].'';?>" alt="" /></a>
	<nav id="navigation" class="hasLogo">
		<ul id="dropmenu" class="menu">
			<li><a href="/">Home</a></li>
			<li><a href="/synopsis/index">Synopsis</a></li>
			<li><a href="/castcrew/index">Cast &#038; Crew</a></li>
			<li><a href="/gallery/index">Gallery</a></li>
			<li><a href="/video/index">Video</a></li>
			<li><a href="/tautan/index">Link</a></li>
			<li><a target="_blank" href="http://www.kaningapictures.com/">Kaninga Pictures</a></li>
		</ul>
	</nav>
<!--WIDGET-->
	<ul id="sidebar-widgets">
		<li class="widget">
			<h2 class="widget-title" style="color: #FFDD17;">What is this?</h2>			
			<div class="textwidget"><?php echo $datapref['narasi']; ?></div>
		</li>
	</ul>

</div><!--end sidebar-->