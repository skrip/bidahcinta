<div id="ajax-content">
	<!--PAGE INFO FOR UPDATING BODY CLASS AND DOC TITLE-->
	<div id="page-info" data-page-title="Videos List | VYSUAL - In Select Theaters December 25" class="archive category category-videos category-5 ajax-on"></div>

	<div class="posts-container">	
		<div id="slide-right" class="slide-nav"><span></span></div>
		<div id="slide-left" class="slide-nav"><span></span></div>
	
		<h1 class="entrytitle">Videos</h1>
	
		<div class="scroll-wrapper">
			<div class="scroll-this">	
				<?php 
					foreach ($data as $value){
						$vid = explode("=", $value['linkvideo']);
						//echo $vid[1];	
						//die;
						echo '<div class="post">
									<div class="image-container">					
										<a class="post-video" data-vidtype="youtube-video" data-vidid="'.$vid[1].'" href="#"><i class="fa fa-play"></i></a>
										<img src="/showfile/show?namafile='.$value['filename'].'" alt="" />
									</div>			
									<div class="postInfo">		
										<h2 class="posttitle">'.$value['title'].'</h2>
										<p>'.$value['description'].'</p>
									</div>
							  </div>';
					}
				?>
			</div><!--end scroll-this-->
		</div><!--end scroll-wrapper-->
	</div><!--end posts-container-->
	
	<!--VIDEO HOLDER-->
	<div class="videoContainer">
		<div class="closeVideo">&times;</div>
	</div>
</div>

<div id="headerImages">
	<?php
		foreach ($dataslider as $key) {
			echo '<div class="header-image" style="background-image:url(/showfile/show?namafile='.$key['filename'].');"><img src="/showfile/show?namafile='.$key['filename'].'" alt=""></div>';
		}
	?>
</div>